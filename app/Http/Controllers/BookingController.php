<?php

namespace App\Http\Controllers;

use App\Booking;
use Illuminate\Http\Request;
use Illuminate\Validator;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('booking');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'origin_pincode' => 'required|numeric',
            'country' => 'required_if:booking_type,==,2',
            'delivery_pincode' => 'required|numeric',
            'weight_in_gm' => 'required_if:shipment_type,==,1',
            'weight_in_kg' => 'required_if:shipment_type,==,2',
            'content_type' => 'required_if:shipment_type,==,2',
            'declared_value' => 'required_if:shipment_type,==,2',
            'length_in_cm' => 'required_if:shipment_type,==,2',
            'breath_in_cm' => 'required_if:shipment_type,==,2',
            'height_in_cm' => 'required_if:shipment_type,==,2',
            'no_of_items' => 'required|numeric',
        ],
        [
            'country.required_if' => 'The country field is required.',
            'weight_in_gm.required_if' => 'The weight (gm) field is required.',
            'weight_in_kg.required_if' => 'The weight (kg) field is required.',
            'content_type.required_if' => 'The content type field is required.',
            'declared_value.required_if' => 'The declared value field is required.',
            'length_in_cm.required_if' => 'The length field is required.',
            'breath_in_cm.required_if' => 'The breath field is required.',
            'height_in_cm.required_if' => 'The height field is required.',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        //
    }
}
