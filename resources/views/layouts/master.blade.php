<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <meta name="keywords" content="Fashion Hub Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Custom Theme files -->
    <link href="{{ asset('css/bootstrap.css') }}" type="text/css" rel="stylesheet" media="all">
    <!-- shop css -->
    <link href="{{ asset('css/shop.css') }}" type="text/css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <!-- Owl-Carousel-CSS -->
    <link href="{{ asset('css/style.css') }}" type="text/css" rel="stylesheet" media="all">
    <!-- font-awesome icons -->
    <link href="{{ asset('css/fontawesome-all.min.css') }}" rel="stylesheet">
    <!-- //Custom Theme files -->
    <!-- online-fonts -->
    <link href="https://fonts.googleapis.com/css?family=Elsie+Swash+Caps:400,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">
    <!-- //online-fonts -->
</head>

<body>
    <div id="app">
    <!-- header -->
        <header>
            <div class="container">
                <!-- top nav -->
                <nav class="top_nav d-flex pt-3 pb-1">
                    <!-- logo -->
                    <h1>
                         <a class="navbar-brand" href="{{ url('/') }}">
                            {{ config('app.name', 'Laravel') }}
                        </a>
                    </h1>
                    <!-- //logo -->
                    <div class="w3ls_right_nav ml-auto d-flex">
                        <!-- search form -->
                        <form class="nav-search form-inline my-0 form-control" action="#" method="post">
                            <select class="form-control input-lg" name="category">
                                <option value="all">Search our store</option>
                                <optgroup label="Mens">
                                    <option value="T-Shirts">T-Shirts</option>
                                    <option value="coats-jackets">Coats & Jackets</option>
                                    <option value="Shirts">Shirts</option>
                                    <option value="Suits & Blazers">Suits & Blazers</option>
                                    <option value="Jackets">Jackets</option>
                                    <option value="Sweat Shirts">Trousers</option>
                                </optgroup>
                                <optgroup label="Womens">
                                    <option value="Dresses">Dresses</option>
                                    <option value="T-shirts">T-shirts</option>
                                    <option value="skirts">Skirts</option>
                                    <option value="jeans">Jeans</option>
                                    <option value="Tunics">Tunics</option>
                                </optgroup>
                                <optgroup label="Girls">
                                    <option value="Dresses">Dresses</option>
                                    <option value="T-shirts">T-shirts</option>
                                    <option value="skirts">Skirts</option>
                                    <option value="jeans">Jeans</option>
                                    <option value="Tops">Tops</option>
                                </optgroup>
                                <optgroup label="Boys">
                                    <option value="T-Shirts">T-Shirts</option>
                                    <option value="coats-jackets">Coats & Jackets</option>
                                    <option value="Shirts">Shirts</option>
                                    <option value="Suits & Blazers">Suits & Blazers</option>
                                    <option value="Jackets">Jackets</option>
                                    <option value="Sweat Shirts">Sweat Shirts</option>
                                </optgroup>
                            </select>
                            <input class="btn btn-outline-secondary  ml-3 my-sm-0" type="submit" value="Search">
                        </form>
                        <!-- search form -->
                        <div class="nav-icon d-flex">
                            <!-- sigin and sign up -->
                            <!-- Authentication Links -->
                            @guest
                                <a class="login_btn align-self-center mx-3" href="{{ route('login') }}">{{ __('Login') }}</a>

                                @if (Route::has('register'))                                    
                                        <a class="login_btn align-self-center mx-3" href="{{ route('register') }}">{{ __('Register') }}</a>
                                @endif

                                <a class="login_btn align-self-center mx-3" href="/booking">{{ __('Guest') }}</a>
                            @else
                                <a class="login_btn align-self-center mx-3" href="#">{{ Auth::user()->name }},</a>
                                <a class="login_btn align-self-center mx-3" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                                
                            @endguest
                            <!-- sigin and sign up -->
                            <!-- shopping cart -->
                            <div class="cart-mainf">
                                <div class="hubcart hubcart2 cart cart box_1">
                                    <form action="#" method="post">
                                        <input type="hidden" name="cmd" value="_cart">
                                        <input type="hidden" name="display" value="1">
                                        <button class="btn top_hub_cart mt-1" type="submit" name="submit" value="" title="Cart">
                                            <i class="fas fa-shopping-bag"></i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                            <!-- //shopping cart ends here -->
                        </div>
                    </div>
                </nav>
                <!-- //top nav -->
                <!-- bottom nav -->
                <nav class="navbar navbar-expand-lg navbar-light justify-content-center">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mx-auto text-center">
                            <li class="nav-item">
                                <a class="nav-link  active" href="/">Home
                                    <span class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/booking">Booking</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="about.html">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="blog.html">Blog</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contact.html">Contact</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <!-- //bottom nav -->
            </div>
            <!-- //header container -->
        </header>
        <!-- //header -->
        <main class="py-4">
            @yield('content')
        </main>
        <!-- footer -->
        <footer>
            <div class="footerv2-w3ls">
                <div class="footer-w3lagile-innerr">
                    <!-- footer-top -->
                    <div class="container-fluid">
                        <div class="row  footer-v2grids w3-agileits">
                            <!-- services -->
                            <div class="col-lg-3 col-sm-6 footer-v2grid">
                                <h4>Support</h4>
                                <ul>

                                    <li>
                                        <a href="payment.html">Payment</a>
                                    </li>
                                    <li>
                                        <a href="#">Shipping</a>
                                    </li>
                                    <li>
                                        <a href="#">Cancellation & Returns</a>
                                    </li>
                                    <li>
                                        <a href="faq.html">FAQ</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- //services -->
                            <!-- latest posts -->
                            <div class="col-lg-3 col-sm-6 footer-v2grid mt-sm-0 mt-5">
                                <h4>Latest Blog</h4>
                                <div class="footer-v2grid1 row">
                                    <div class="col-4 footer-v2grid1-left">
                                        <a href="blog.html">
                                            <img src="{{ asset('images/bl2.jpg') }}" alt=" " class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="col-8 footer-v2grid1-right p-0">
                                        <a href="blog.html">eveniie arcet ut moles morbi dapiti</a>
                                    </div>
                                </div>
                                <div class="footer-v2grid1 row my-2">
                                    <div class="col-4 footer-v2grid1-left">
                                        <a href="blog.html">
                                            <img src="{{ asset('images/bl1.jpg') }}" alt=" " class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="col-8 footer-v2grid1-right p-0">
                                        <a href="blog.html">earum rerum tenmorbi dapiti et</a>
                                    </div>
                                </div>
                                <div class="footer-v2grid1 row">
                                    <div class="col-4 footer-v2grid1-left">
                                        <a href="blog.html">
                                            <img src="{{ asset('images/bl3.jpg') }}" alt=" " class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="col-8 footer-v2grid1-right p-0">
                                        <a href="blog.html">morbi dapiti eveniet ut molesti</a>
                                    </div>
                                </div>
                            </div>
                            <!-- //latest posts -->
                            <!-- locations -->
                            <div class="col-lg-3 col-sm-6 footer-v2grid my-lg-0 my-5">
                                <h4>office locations</h4>
                                <ul>
                                    <li>
                                        <a href="#">new jersey</a>
                                    </li>
                                    <li>
                                        <a href="#">texas</a>
                                    </li>
                                    <li>
                                        <a href="#">michigan</a>
                                    </li>
                                    <li>
                                        <a href="#">cannada</a>
                                    </li>
                                    <li>
                                        <a href="#">brazil</a>
                                    </li>
                                    <li>
                                        <a href="#">california</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- //locations -->
                            <!-- popular tags -->
                            <div class="col-lg-3  footer-v2grid mt-sm-0 mt-5">
                                <h4>popular tags</h4>
                                <ul class="w3-tag2">
                                    <li>
                                        <a href="shop.html">amet</a>
                                    </li>
                                    <li>
                                        <a href="men.html">placerat</a>
                                    </li>
                                    <li>
                                        <a href="shop.html">Proin </a>
                                    </li>
                                    <li>
                                        <a href="boys.html">vehicula</a>
                                    </li>
                                    <li>
                                        <a href="shop.html">diam</a>
                                    </li>
                                    <li>
                                        <a href="women.html">velit</a>
                                    </li>
                                    <li>
                                        <a href="shop.html">felis</a>
                                    </li>
                                    <li>
                                        <a href="shop.html">mauris</a>
                                    </li>
                                    <li>
                                        <a href="girls.html">amet</a>
                                    </li>
                                    <li>
                                        <a href="shop.html">placerat</a>
                                    </li>
                                    <li>
                                        <a href="shop.html">Proin </a>
                                    </li>
                                    <li>
                                        <a href="index.html">vehicula</a>
                                    </li>
                                    <li>
                                        <a href="shop.html">diam</a>
                                    </li>
                                    <li>
                                        <a href="men.html">velit</a>
                                    </li>
                                    <li>
                                        <a href="shop.html">felis</a>
                                    </li>
                                    <li>
                                        <a href="women.html">mauris</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- //popular tags -->
                        </div>
                    </div>
                    <!-- //footer-top -->
                    <div class="footer-bottomv2 py-5">
                        <div class="container">
                            <ul class="bottom-links-agile">
                                <li>
                                    <a href="index.html">Home</a>
                                </li>
                                <li>
                                    <a href="about.html">About Us</a>
                                </li>
                                <li>
                                    <a href="shop.html">Shop</a>
                                </li>
                                <li>
                                    <a href="contact.html">Contact</a>
                                </li>

                            </ul>
                            <h3 class="text-center follow">Follow Us</h3>
                            <ul class="social-iconsv2 agileinfo">
                                <li>
                                    <a href="#">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="fab fa-youtube"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-linkedin-in"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="container-fluid py-5 footer-copy_w3ls">
                    <div class="d-lg-flex justify-content-between">
                        <div class="mt-2 sub-some align-self-lg-center">
                            <h5>Payment Method</h5>
                            <ul class="mt-4">
                                <li class="list-inline-item">
                                    <img src="{{ asset('images/pay2.png') }}" alt="">
                                </li>
                                <li class="list-inline-item">
                                    <img src="{{ asset('images/pay5.png') }}" alt="">
                                </li>
                                <li class="list-inline-item">
                                    <img src="{{ asset('images/pay3.png') }}" alt="">
                                </li>
                                <li class="list-inline-item">
                                    <img src="{{ asset('images/pay7.png') }}" alt="">
                                </li>
                                <li class="list-inline-item">
                                    <img src="{{ asset('images/pay8.png') }}" alt="">
                                </li>
                                <li class="list-inline-item ">
                                    <img src="{{ asset('images/pay9.png') }}" alt="">
                                </li>
                            </ul>
                        </div>
                        <div class="cpy-right align-self-center">
                            <h2 class="agile_btxt">
                                <a href="/">
                                    <span>P</span>ool<span>P</span>arcel</a>
                            </h2>
                            <p>© 2020 Pool Parcel. All rights reserved | Design by
                                <a href="" class="text-secondary"> Intensofy.</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- //footer -->     
        <!-- js -->
        <script src="{{ asset('js/jquery-2.2.3.min.js') }}"></script>
        <script src="{{ asset('js/customJs.js') }}"></script>
        <!-- //js -->
        <!-- smooth dropdown -->
        <script>
            $(document).ready(function () {
                $('ul li.dropdown').hover(function () {
                    $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
                }, function () {
                    $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
                });
            });
        </script>
        <!-- //smooth dropdown -->
        <!-- Banner Responsiveslides -->
        <script src="{{ asset('js/responsiveslides.min.js') }}"></script>
        <script>
            // You can also use "$(window).load(function() {"
            $(function () {
                // Slideshow 4
                $("#slider3").responsiveSlides({
                    auto: false,
                    pager: true,
                    nav: false,
                    speed: 500,
                    namespace: "callbacks",
                    before: function () {
                        $('.events').append("<li>before event fired.</li>");
                    },
                    after: function () {
                        $('.events').append("<li>after event fired.</li>");
                    }
                });

            });
        </script>
        <!-- // Banner Responsiveslides -->
        <!-- Product slider Owl-Carousel-JavaScript -->
        <script src="{{ asset('js/owl.carousel.js') }}"></script>
        <script>
            var owl = $('.owl-carousel');
            owl.owlCarousel({
                items: 4,
                loop: false,
                margin: 10,
                autoplay: false,
                autoplayTimeout: 5000,
                autoplayHoverPause: false,
                responsive: {
                    320: {
                        items: 1,
                    },
                    568: {
                        items: 2,
                    },
                    991: {
                        items: 3,
                    },
                    1050: {
                        items: 4
                    }
                }
            });
        </script>
        <!-- //Product slider Owl-Carousel-JavaScript -->
        <!-- cart-js -->
        <script src="{{ asset('js/minicart.js') }}">
        </script>
        <script>
            hub.render();

            hub.cart.on('new_checkout', function (evt) {
                var items, len, i;

                if (this.subtotal() > 0) {
                    items = this.items();

                    for (i = 0, len = items.length; i < len; i++) {}
                }
            });
        </script>
        <!-- //cart-js -->
        <!-- start-smooth-scrolling -->
        <script src="{{ asset('js/move-top.js') }}"></script>
        <script src="{{ asset('js/easing.js') }}"></script>
        <script>
            jQuery(document).ready(function ($) {
                $(".scroll").click(function (event) {
                    event.preventDefault();

                    $('html,body').animate({
                        scrollTop: $(this.hash).offset().top
                    }, 1000);
                });
            });
        </script>
        <!-- //end-smooth-scrolling -->
        <!-- smooth-scrolling-of-move-up -->
        <script>
            $(document).ready(function () {
                /*
                var defaults = {
                    containerID: 'toTop', // fading element id
                    containerHoverID: 'toTopHover', // fading element hover id
                    scrollSpeed: 1200,
                    easingType: 'linear' 
                };
                */

                $().UItoTop({
                    easingType: 'easeOutQuart'
                });

            });
        </script>
        <script src="{{ asset('js/SmoothScroll.min.js') }}"></script>
        <!-- //smooth-scrolling-of-move-up -->
        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="{{ asset('js/bootstrap.js') }}"></script>
    </div>
</body>

</html>