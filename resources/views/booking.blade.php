@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Booking') }}</div>

                <div class="card-body">
                    <form method="POST" action="/booking">
                        @csrf

                        <div class="form-group row">
                            <label for="booking_type" class="col-md-4 col-form-label text-md-right">{{ __('Booking Type') }}</label>

                            <div class="col-md-6">
                                <select id="booking_type" name="booking_type" class="form-control">
                                    <option value="1" {{ old('booking_type') == 1 ? 'selected' : '' }}>Demestic</option>
                                    <option value="2" {{ old('booking_type') == 2 ? 'selected' : '' }}>International</option>
                                </select>
                            </div>
                        </div>   

                        <div class="form-group row">
                            <label for="origin_pincode" class="col-md-4 col-form-label text-md-right">{{ __('Origin Pin/Zip Code') }}<span class="text-danger"> *</span></label>

                            <div class="col-md-6">
                                <input type="text" id="origin_pincode" class="form-control @error('origin_pincode') is-invalid @enderror" name="origin_pincode" value="{{ old('origin_pincode') }}">

                                @error('origin_pincode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row" id="country_div" @if(old('booking_type') == 2) style="display: flex;" @endif style="display: none;">
                            <label for="booking_type" class="col-md-4 col-form-label text-md-right">{{ __('Country') }}<span class="text-danger"> *</span></label>

                            <div class="col-md-6">
                                <select id="country" name="country" class="form-control">
                                    <option value="">Select Country</option>
                                    <option value="IN" {{ old('country') == 'IN' ? 'selected' : '' }}>India</option>
                                    <option value="PAK" {{ old('country') == 'PAK' ? 'selected' : '' }}>Pakistan</option>
                                    <option value="US" {{ old('country') == 'US' ? 'selected' : '' }}>United State</option>
                                </select>

                                @error('country')
                                    <span class="invalid-feedback" role="alert" @if(old('booking_type') == 2) style="display: block;" @endif>
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> 

                        <div class="form-group row">
                            <label for="delivery_pincode" class="col-md-4 col-form-label text-md-right">{{ __('Delivery Pin/Zip Code') }}<span class="text-danger"> *</span></label>

                            <div class="col-md-6">
                                <input type="text" id="delivery_pincode" class="form-control @error('delivery_pincode') is-invalid @enderror" name="delivery_pincode" value="{{ old('delivery_pincode') }}">

                                @error('delivery_pincode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div> 
                        <hr>
                        <div class="form-group row">
                            <label for="shipment_type" class="col-md-4 col-form-label text-md-right">{{ __('Shipment Type') }}</label>

                            <div class="col-md-6">
                                <select id="shipment_type" name="shipment_type" class="form-control">
                                    <option value="1" {{ old('shipment_type') == 1 ? 'selected' : '' }}>Document</option>
                                    <option value="2" {{ old('shipment_type') == 2 ? 'selected' : '' }}>Non-Document</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row" id="document_div" @if(old('shipment_type') == 2) style="display: none;" @endif>
                            <label for="weight_in_gm" class="col-md-4 col-form-label text-md-right">{{ __('Weight (gm)') }}<span class="text-danger"> *</span></label>

                            <div class="col-md-6">
                                <input type="text" id="weight_in_gm" class="form-control @error('weight_in_gm') is-invalid @enderror" name="weight_in_gm" value="{{ old('weight_in_gm') }}">

                                @error('weight_in_gm')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div id="non_document_div" {{ old('shipment_type') == 2 ? 'style="display: block;"' : '' }} style="display: none;">
                            <div class="form-group row">
                                <label for="weight_in_kg" class="col-md-4 col-form-label text-md-right">{{ __('Weight (kg)') }}<span class="text-danger"> *</span></label>

                                <div class="col-md-6">
                                    <input type="text" id="weight_in_kg" class="form-control @error('weight_in_kg') is-invalid @enderror" name="weight_in_kg" value="{{ old('weight_in_kg') }}">

                                    @error('weight_in_kg')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="content_type" class="col-md-4 col-form-label text-md-right">{{ __('Content Type') }}<span class="text-danger"> *</span></label>

                                <div class="col-md-6">
                                    <input type="text" id="content_type" class="form-control @error('content_type') is-invalid @enderror" name="content_type" value="{{ old('content_type') }}">

                                    @error('content_type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="declared_value" class="col-md-4 col-form-label text-md-right">{{ __('Declared Value ($/Rs.)') }}<span class="text-danger"> *</span></label>

                                <div class="col-md-6">
                                    <input type="text" id="declared_value" class="form-control @error('declared_value') is-invalid @enderror" name="declared_value" value="{{ old('declared_value') }}">

                                    @error('declared_value')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="length_in_cm" class="col-md-4 col-form-label text-md-right">{{ __('Length (cm)') }}<span class="text-danger"> *</span></label>

                                <div class="col-md-6">
                                    <input type="text" id="length_in_cm" class="form-control @error('length_in_cm') is-invalid @enderror" name="length_in_cm" value="{{ old('length_in_cm') }}">

                                    @error('length_in_cm')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="breath_in_cm" class="col-md-4 col-form-label text-md-right">{{ __('Breath (cm)') }}<span class="text-danger"> *</span></label>

                                <div class="col-md-6">
                                    <input type="text" id="breath_in_cm" class="form-control @error('breath_in_cm') is-invalid @enderror" name="breath_in_cm" value="{{ old('breath_in_cm') }}">

                                    @error('breath_in_cm')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="height_in_cm" class="col-md-4 col-form-label text-md-right">{{ __('Height (cm)') }}<span class="text-danger"> *</span></label>

                                <div class="col-md-6">
                                    <input type="text" id="height_in_cm" class="form-control @error('height_in_cm') is-invalid @enderror" name="height_in_cm" value="{{ old('height_in_cm') }}">

                                    @error('height_in_cm')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="no_of_items" class="col-md-4 col-form-label text-md-right">{{ __('Number of items') }}<span class="text-danger"> *</span></label>

                            <div class="col-md-6">
                                <input type="text" id="no_of_items" class="form-control @error('no_of_items') is-invalid @enderror" name="no_of_items" value="{{ old('no_of_items') }}">

                                @error('no_of_items')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Confirm') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
